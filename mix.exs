defmodule DeclarativeDNS.MixProject do
	use Mix.Project

	def project do
		[
			app: :declarative_dns,
			version: "0.1.0",
			elixir: "~> 1.13",
			start_permanent: Mix.env() == :prod,
			deps: deps()
		]
	end

	def application do
		[
			extra_applications: [:logger],
			mod: {DeclarativeDNS.Application, [env: Mix.env()]}
		]
	end

	defp deps do
		[
			{:bonny, "~> 1.0"},
			{:pluggable, "~> 1.0"},
			{:req, "~> 0.3.1"},
		]
	end
end
