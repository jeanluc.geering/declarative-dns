defmodule DeclarativeDNS.Application do
	use Application

	def start(_type, env: env) do
		children = [
			{DeclarativeDNS.Operator, conn: DeclarativeDNS.K8sConn.get(env)},
		]

		opts = [strategy: :one_for_one, name: DeclarativeDNS.Supervisor]
		Supervisor.start_link(children, opts)
	end

end
