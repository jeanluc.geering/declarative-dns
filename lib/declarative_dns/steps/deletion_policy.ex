defmodule DeclarativeDNS.Steps.DeletionPolicy do
	require Logger
	@log "#{__MODULE__} - " |> String.replace_leading("Elixir.", "")

	alias Bonny.Axn

	@behaviour Pluggable

	@impl Pluggable
	def init(opts), do: opts

	@impl Pluggable
	def call(%Axn{resource: resource, action: :delete} = axn, _) do
		case get_deletion_policy(resource) do
			"abandon" ->
				Logger.info(@log <> "skipping deletion of #{full_name(resource)}")
				Pluggable.Token.halt(axn)
			_ ->
				axn
		end
	end
	def call(axn, _), do: axn

	defp full_name(%{"kind" => kind, "metadata" => %{"namespace" => ns, "name" => name}}) do
		"#{ns}/#{kind}/#{name}"
	end

	defp get_deletion_policy(resource) do
		get_in(resource, ["metadata", "annotations", "declarative-dns.net/deletion-policy"])
	end
end
