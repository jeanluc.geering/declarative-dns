defmodule DeclarativeDNS.Provider do

	alias DeclarativeDNS.Providers

	def lookup(name) do
		case name do
			"dnsimple" -> Providers.DNSimple
			"ns1" -> Providers.NS1
		end
	end
end
