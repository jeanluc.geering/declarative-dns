defmodule DeclarativeDNS.API.V1Alpha1.Contact do

	use Bonny.API.Version,
		hub: true

	@providers [
		"dnsimple",
	]

	@impl true
	def manifest() do
		struct!(
			defaults(),
			schema: %{
				openAPIV3Schema: %{
					type: :object,
					properties: %{
						spec: %{
							type: :object,
							properties: %{
								provider: %{
									type: :string,
									enum: @providers,
									# FEATURE STATE: Kubernetes v1.25 [beta]
									'x-kubernetes-validations': [
										# transition rule
										%{ rule: "self == oldSelf", message: "read-only" },
									],
								},
								id: %{ type: :integer },
								first_name: %{ type: :string },
								last_name: %{ type: :string },
								organization_name: %{ type: :string },
								job_title: %{ type: :string },
								address1: %{ type: :string },
								address2: %{ type: :string },
								postal_code: %{ type: :string },
								city: %{ type: :string },
								state_province: %{ type: :string },
								country: %{ type: :string },
								email: %{ type: :string },
								phone: %{ type: :string },
							},
							required: [
								"provider",
								"first_name",
								"last_name",
								"address1",
								"postal_code",
								"city",
								"state_province",
								"country",
								"email",
								"phone",
							],
						},
						status: %{
							type: :object,
							properties: %{
								conditions: %{
									type: :array,
									'x-kubernetes-list-type': "map",
									'x-kubernetes-list-map-keys': ["type"],
									items: %{
										type: :object,
										properties: %{
											lastTransitionTime: %{ type: :string, format: "date-time" },
											type: %{ type: :string },
											status:  %{ type: :string, enum: ["True", "False", "Unknown"] },
											reason: %{ type: :string },
											message: %{ type: :string },
										},
										required: ["status", "type"],
									}
								},
								contact: %{
									type: :object,
									properties: %{
										id: %{ type: :integer },
									},
									required: ["id"],
								},
							},
						},
					},
					required: ["spec"],
				}
			},
			additionalPrinterColumns: [
				%{name: "provider", type: :string, description: "Provider", jsonPath: ".spec.provider"},
			],
			subresources: %{ status: %{} }
		)
		|> add_observed_generation_status()
	end

end
