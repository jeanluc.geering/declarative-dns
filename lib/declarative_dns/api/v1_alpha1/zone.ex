defmodule DeclarativeDNS.API.V1Alpha1.Zone do

	use Bonny.API.Version,
		hub: true

	# TODO move to some other module
	@providers [
		"aws_route_53",
		"cloudflare",
		"digital_ocean",
		"dnsimple",
		"dnsmadeeasy",
		"google_cloud_dns",
		"ns1",
	]

	@impl true
	def manifest() do
		struct!(
			defaults(),
			schema: %{
				openAPIV3Schema: %{
					type: :object,
					properties: %{
						spec: %{
							type: :object,
							properties: %{
								provider: %{
									type: :string,
									enum: @providers,
									# FEATURE STATE: Kubernetes v1.25 [beta]
									'x-kubernetes-validations': [
										# transition rule
										%{ rule: "self == oldSelf", message: "read-only" },
									],
								},
								name: %{
									type: :string,
									# FEATURE STATE: Kubernetes v1.25 [beta]
									'x-kubernetes-validations': [
										# transition rule
										%{ rule: "self == oldSelf", message: "read-only" },
									],
								},
							},
							required: ["provider", "name"],
						},
						status: %{
							type: :object,
							properties: %{
								conditions: %{
									type: :array,
									'x-kubernetes-list-type': "map",
									'x-kubernetes-list-map-keys': ["type"],
									items: %{
										type: :object,
										properties: %{
											lastTransitionTime: %{ type: :string, format: "date-time" },
											type: %{ type: :string },
											status:  %{ type: :string, enum: ["True", "False", "Unknown"] },
											reason: %{ type: :string },
											message: %{ type: :string },
										},
										required: ["status", "type"],
									}
								},
							},
						}
					},
					required: ["spec"],
				}
			},
			additionalPrinterColumns: [
				%{name: "provider", type: :string, description: "Provider", jsonPath: ".spec.provider"},
				%{name: "name", type: :string, description: "Name", jsonPath: ".spec.name"},
			],
			subresources: %{ status: %{} }
		)
		|> add_observed_generation_status()
	end

end
