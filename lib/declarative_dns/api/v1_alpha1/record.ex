defmodule DeclarativeDNS.API.V1Alpha1.Record do

	use Bonny.API.Version,
		hub: true

	@providers [
		"dnsimple",
	]

	@impl true
	def manifest() do
		struct!(
			defaults(),
			schema: %{
				openAPIV3Schema: %{
					type: :object,
					properties: %{
						spec: %{
							type: :object,
							properties: %{
								provider: %{
									type: :string,
									enum: @providers,
									# FEATURE STATE: Kubernetes v1.25 [beta]
									'x-kubernetes-validations': [
										# transition rule
										%{ rule: "self == oldSelf", message: "read-only" },
									],
								},
								zone: %{
									type: :string,
									# FEATURE STATE: Kubernetes v1.25 [beta]
									'x-kubernetes-validations': [
										# transition rule
										%{ rule: "self == oldSelf", message: "read-only" },
									],
								},
								name: %{
									type: :string,
									# FEATURE STATE: Kubernetes v1.25 [beta]
									'x-kubernetes-validations': [
										# transition rule
										%{ rule: "self == oldSelf", message: "read-only" },
									],
								},
								type: %{
									type: :string,
									# FEATURE STATE: Kubernetes v1.25 [beta]
									'x-kubernetes-validations': [
										# transition rule
										%{ rule: "self == oldSelf", message: "read-only" },
									],
								},
								ttl: %{ type: :integer },
								content: %{ type: :string },
							},
							required: ["provider", "zone", "name", "type", "content"],
						},
						status: %{
							type: :object,
							properties: %{
								conditions: %{
									type: :array,
									'x-kubernetes-list-type': "map",
									'x-kubernetes-list-map-keys': ["type"],
									items: %{
										type: :object,
										properties: %{
											lastTransitionTime: %{ type: :string, format: "date-time" },
											type: %{ type: :string },
											status:  %{ type: :string, enum: ["True", "False", "Unknown"] },
											reason: %{ type: :string },
											message: %{ type: :string },
										},
										required: ["status", "type"],
									}
								},
							},
						}
					},
					required: ["spec"],
				}
			},
			additionalPrinterColumns: [
				%{name: "provider", type: :string, description: "Provider", jsonPath: ".spec.provider"},
				%{name: "zone", type: :string, description: "Zone", jsonPath: ".spec.zone"},
				%{name: "name", type: :string, description: "Name", jsonPath: ".spec.name"},
				%{name: "type", type: :string, description: "Type", jsonPath: ".spec.type"},
				%{name: "ttl", type: :string, description: "TTL", jsonPath: ".spec.ttl"},
				%{name: "content", type: :string, description: "Content", jsonPath: ".spec.content"},
			],
			subresources: %{ status: %{} }
		)
		|> add_observed_generation_status()
	end

end
