defmodule DeclarativeDNS.Providers.NS1.Client do

	@base [
		base_url: "https://api.nsone.net/v1",
		headers: [
			{"X-NSONE-Key", System.get_env("NSONE_API_KEY")},
		],
	]

	def get(%Req.Request{} = request) do
		case request(request) do
			{:ok, body} -> body
			{:error, :not_found} -> nil
		end
	end

	def request(%Req.Request{} = request) do
		case Req.request(request, @base) do
			{:ok, %Req.Response{status: 200, body: body}} -> {:ok, body}
			{:ok, %Req.Response{status: 400, body: %{"message" => message}}} -> {:error, :api, message}
			{:ok, %Req.Response{status: 403}} -> {:error, :access_denied}
			{:ok, %Req.Response{status: 404}} -> {:error, :not_found}
		end
	end

end
