defmodule DeclarativeDNS.Providers.NS1.Record do

	alias DeclarativeDNS.Data.RecordSet
	alias DeclarativeDNS.Providers.NS1.Client

	def get(record) when is_struct(record, RecordSet) do
		Req.new(url: url(record)) |> Client.get()
	end

	def apply(record) when is_struct(record, RecordSet) do

		url = url(record)

		method = case Req.new(url: url) |> Client.request() do
			{:ok, _} -> :post
			{:error, :not_found} -> :put
		end

		answers = for value <- record.values do
			%{"answer" => [value]}
		end

		[
			url: url,
			method: method,
			json: %{
				"zone" => record.zone,
				"domain" => record.domain,
				"type" => record.type,
				"answers" => answers,
				"ttl" => record.ttl,
				# only useful in advanced use cases (filtering)
				"use_client_subnet" => false,
			}
		]
		|> Req.new()
		|> Client.request()
	end

	def delete(record) when is_struct(record, RecordSet) do
		Req.new(url: url(record), method: :delete)
		|> Client.request()
		|> case do
			{:ok, _} -> :ok
			{:error, :not_found} -> :ok
		end
	end

	defp url(record), do: "zones/#{record.zone}/#{record.domain}/#{record.type}"
end
