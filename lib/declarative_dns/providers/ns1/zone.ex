defmodule DeclarativeDNS.Providers.NS1.Zone do

	alias DeclarativeDNS.Data.Zone
	alias DeclarativeDNS.Providers.NS1.Client

	def list() do
		Req.new(url: "zones") |> Client.get()
	end

	def get(zone) when is_struct(zone, Zone) do
		Req.new(url: url(zone)) |> Client.get()
	end

	def records(zone) when is_struct(zone, Zone) do
		case Req.new(url: url(zone)) |> Client.request() do
			{:ok, %{"records" => records}} -> {:ok, records}
			other -> other
		end
	end

	def apply(zone) when is_struct(zone, Zone) do

		url = url(zone)

		method = case Req.new(url: url) |> Client.request() do
			{:ok, _} -> :post
			{:error, :not_found} -> :put
		end

		[
			url: url,
			method: method,
			json: %{
				"zone" => zone.fqdn || zone.name,
			}
		]
		|> Req.new()
		|> Client.request()
	end

	def delete(zone) when is_struct(zone, Zone) do
		Req.new(url: url(zone), method: :delete)
		|> Client.request()
		|> case do
			{:ok, _} -> :ok
			{:error, :not_found} -> :ok
		end
	end

	defp url(zone) when is_struct(zone, Zone), do: "zones/#{zone.name}"

end
