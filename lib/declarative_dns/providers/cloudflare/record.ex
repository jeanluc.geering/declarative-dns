defmodule DeclarativeDNS.Providers.Cloudflare.Record do

	# alias DeclarativeDNS.Data.Record
	alias DeclarativeDNS.Providers.Cloudflare.Client

	def list(zone_id) when is_binary(zone_id) do
		case Req.new(url: "zones/#{zone_id}/dns_records") |> Client.request() do
			# TODO deal with pagination
			{:ok, %{"result" => records, "result_info" => %{"total_pages" => 1}}} -> {:ok, records}
			# other -> other
		end
	end

end
