defmodule DeclarativeDNS.Providers.Cloudflare.Account do

	alias DeclarativeDNS.Providers.Cloudflare.Client

	def list() do
		Req.new(url: "accounts") |> Client.get()
	end

end
