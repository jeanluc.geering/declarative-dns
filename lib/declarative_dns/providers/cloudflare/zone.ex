defmodule DeclarativeDNS.Providers.Cloudflare.Zone do

	alias DeclarativeDNS.Data.Zone
	alias DeclarativeDNS.Providers.Cloudflare.Client
	alias DeclarativeDNS.Providers.Cloudflare.Record

	def list() do
		Req.new(url: "zones") |> Client.get()
	end

	def get(zone) when is_struct(zone, Zone) and not is_nil(zone.id), do: get(zone.id)
	def get(zone_id) when is_binary(zone_id) do
		Req.new(url: "zones/#{zone_id}") |> Client.get()
	end

	def records(zone) when is_struct(zone, Zone) and not is_nil(zone.id) do
		Record.list(zone.id)
	end

end
