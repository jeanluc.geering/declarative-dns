defmodule DeclarativeDNS.Providers.Cloudflare.Client do
	# require Logger
	# @log "#{__MODULE__} - " |> String.replace_leading("Elixir.", "")

	# TODO this is read at compile time
	@base [
		base_url: "https://api.cloudflare.com/client/v4/",
		auth: {:bearer, System.fetch_env!("CLOUDFLARE_TOKEN")},
		retry: :never,
	]

	def get(%Req.Request{} = request) do
		case request(request) do
			{:ok, body} -> body
			{:error, :not_found} -> nil
		end
	end

	def request(%Req.Request{} = request) do
		Req.request(request, @base)
		# |> IO.inspect()
		|> case do
			{:ok, %Req.Response{status: 200, body: body}} -> {:ok, body}
			# {:ok, %Req.Response{status: 201, body: body}} -> {:created, body}
			# {:ok, %Req.Response{status: 204}} -> :ok
			{:ok, %Req.Response{status: 400, body: body}} -> {:error, body}
			{:ok, %Req.Response{status: 404}} -> {:error, :not_found}

			# The global rate limit for Cloudflare’s API is 1200 requests per five minutes
			# see https://developers.cloudflare.com/api/reference/limits/
			{:ok, %Req.Response{status: 429}} -> {:error, :rate_limited}
		end
	end

end
