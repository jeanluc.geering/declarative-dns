defmodule DeclarativeDNS.Providers.DNSimple.Record do

	require Logger
	@log "#{__MODULE__} - " |> String.replace_leading("Elixir.", "")

	alias DeclarativeDNS.Data.Record
	alias DeclarativeDNS.Providers.DNSimple.Client

	defp find_by_name_and_type(record) when is_struct(record, Record) do
		url = "zones/#{record.zone}/records?name=#{record.name}&type=#{record.type}"
		{:ok, %{"data" => records}} =
			[url: url]
			|> Req.new()
			|> Client.request()

		records
	end

	def with_id(record) when is_struct(record, Record) and is_nil(record.id) do
		record
		|> find_by_name_and_type()
		|> Enum.find(&(&1["content"] == record.content))
		|> case do
			%{"id" => id} -> %{record | id: id}
			_ -> record
		end
	end
	def with_id(record) when is_struct(record, Record), do: record

	def apply(record) when is_struct(record, Record) do
		record = with_id(record)

		case record.id do
			nil -> create(record)
			_ -> update(record)
		end
	end

	defp create(record) when is_struct(record, Record) and is_nil(record.id) do
		Logger.info(@log <> "create #{record.zone}/#{record.name}/#{record.type}")
		[
			url: "zones/#{record.zone}/records",
			method: :post,
			json: %{
				"name" => record.name,
				"type" => record.type,
				"content" => record.content,
				"ttl" => record.ttl,
				# "priority" => todo,
			}
		]
		|> Req.new()
		|> Client.request()
		|> case do
			{:created, %{"data" => %{"id" => id}}} ->  {:ok, %{record | id: id}}
			# other -> other
		end
	end

	defp update(record) when is_struct(record, Record) and not is_nil(record.id) do
		Logger.debug(@log <> "update #{record.zone}/#{record.name}/#{record.type}")
		[
			url: "zones/#{record.zone}/records/#{record.id}",
			method: :patch,
			json: %{
				"name" => record.name,
				"type" => record.type,
				"content" => record.content,
				"ttl" => record.ttl,
				# "priority" => todo,
			}
		]
		|> Req.new()
		|> Client.request()
		# |> IO.inspect()
		|> case do
			{:ok, %{"data" => _}} ->  {:ok, record}
			{:error, :not_found} -> record |> struct!(id: nil) |> create()
			# other -> other
		end
	end

	def delete(record) when is_struct(record, Record) do
		record = with_id(record)

		case record.id do
			nil -> :ok # not found => nothing to delete
			record_id -> delete(record.zone, record_id)
		end
	end

	def delete(zone, record_id) when is_binary(zone) and is_integer(record_id) do
		Logger.info(@log <> "delete #{zone}/records/#{record_id}")
		Req.new(url: "zones/#{zone}/records/#{record_id}", method: :delete)
		|> Client.request()
		|> case do
			:ok -> :ok
			{:error, :not_found} -> :ok
		end
	end

end
