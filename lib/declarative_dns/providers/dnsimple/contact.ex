defmodule DeclarativeDNS.Providers.DNSimple.Contact do

	alias DeclarativeDNS.Data.Contact
	alias DeclarativeDNS.Providers.DNSimple.Client

	def list() do
		Req.new(url: "contacts") |> Client.get()
	end

	def get(contact) when is_struct(contact, Contact) and not is_nil(contact.id) do
		Req.new(url: "contacts/#{contact.id}") |> Client.get()
	end

	def apply(contact) when is_struct(contact, Contact) and is_nil(contact.id), do: create(contact)
	def apply(contact) when is_struct(contact, Contact), do: update(contact)

	defp create(contact) do
		[
			url: "contacts",
			method: :post,
			json: contact |> Map.from_struct() |> Map.reject(fn {_, v} -> is_nil(v) end)
		]
		|> Req.new()
		|> Client.request()
		|> case do
			{:created, %{"data" => %{"id" => id}}} ->  {:ok, %{contact | id: id}}
			# other -> other
		end
	end
	defp update(contact) do
		[
			url: "contacts/#{contact.id}",
			method: :patch,
			json: %{contact | id: nil} |> Map.from_struct() |> Map.reject(fn {_, v} -> is_nil(v) end)
		]
		|> Req.new()
		|> Client.request()
		|> case do
			{:ok, %{"data" => _}} ->  {:ok, contact}
			{:error, :not_found} -> {:error, :not_found} # contact |> struct!(id: nil) |> create()
			# other -> other
		end
	end

	def delete(contact_id) when is_integer(contact_id) do
		Req.new(url: "contacts/#{contact_id}", method: :delete)
		|> Client.request()
		|> case do
			:ok -> :ok
			{:error, :not_found} -> :ok
		end
	end

	def delete(contact) when is_struct(contact, Contact) and not is_nil(contact.id) do
		delete(contact.id)
	end

end
