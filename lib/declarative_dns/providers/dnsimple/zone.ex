defmodule DeclarativeDNS.Providers.DNSimple.Zone do

	alias DeclarativeDNS.Data.Zone
	alias DeclarativeDNS.Providers.DNSimple.Client

	def list() do
		Req.new(url: "zones") |> Client.get()
	end

	def get(zone) when is_struct(zone, Zone) do
		Req.new(url: "zones/#{zone.name}") |> Client.get()
	end

	def records(zone) when is_struct(zone, Zone) do
		case Req.new(url: "zones/#{zone.name}/records") |> Client.request() do
			# TODO deal with pagination
			{:ok, %{"data" => records}} -> {:ok, records}
			# other -> other
		end
	end

	def apply(zone) when is_struct(zone, Zone) do
		Req.new(url: "zones/#{zone.name}")
		|> Client.request()
		|> case do
			{:ok, %{"data" => %{"name" => name, "id" => id}}} -> {:ok, %{zone | id: id, name: name}}
			{:error, :not_found} -> create(zone)
		end
	end

	defp create(zone) do
		[
			url: "domains",
			method: :post,
			json: %{
				"name" => zone.name,
			}
		]
		|> Req.new()
		|> Client.request()
		|> case do
			{:created, %{"data" => %{"name" => name, "id" => id}}} ->  {:ok, %{zone | id: id, name: name}}
			# other -> other
		end
	end

	def delete(zone) when is_struct(zone, Zone) do
		Req.new(url: "domains/#{zone.name}", method: :delete)
		|> Client.request()
		|> case do
			:ok -> :ok
			{:error, :not_found} -> :ok
		end
	end

end
