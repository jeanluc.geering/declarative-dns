defmodule DeclarativeDNS.Providers.DNSimple.Domain do

	alias DeclarativeDNS.Data.Domain
	alias DeclarativeDNS.Providers.DNSimple.Client

	def list() do
		Req.new(url: "domains") |> Client.get()
	end

	def get(domain) when is_struct(domain, Domain) do
		Req.new(url: "domains/#{domain.name}") |> Client.get()
	end

	def apply(domain) when is_struct(domain, Domain) do
		Req.new(url: "domains/#{domain.name}")
		|> Client.request()
		|> case do
			# not found
			{:error, :not_found} -> register(domain)
			# found but not registered
			{:ok, %{"data" => %{"state" => "hosted"}}} -> register(domain)
			# todo
			# {:ok, %{"data" => %{...}}} -> {:ok, %{domain | id: id, name: name}}
		end
	end

	defp register(domain) do
		[
			url: "registrar/domains/#{domain.name}/registrations",
			method: :post,
			json: %{
				"registrant_id" => domain.contact_id,
				"whois_privacy" => true,
				"auto_renew" => domain.auto_renew,
				# "extended_attributes" => ?
				# "premium_price" => "?"
			}
		]
		|> Req.new()
		|> Client.request()
		|> case do
			# {:error, %{"message" => "TLD _____ does not support WHOIS privacy"}} -> register(privacy: false)
			# {:created, %{"data" => %{"domain_id" => id}}} ->  {:ok, %{domain | id: id, name: name}}
			other -> other
		end
	end

	# TODO make sure auto-renew is set to false
	# def delete(domain) when is_struct(domain, Domain) do
	# 	Req.new(url: "domains/#{domain.name}", method: :delete)
	# 	|> Client.request()
	# 	|> case do
	# 		:ok -> :ok
	# 		{:error, :not_found} -> :ok
	# 	end
	# end

end
