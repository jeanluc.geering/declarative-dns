defmodule DeclarativeDNS.Providers.DNSimple.Client do
	require Logger
	@log "#{__MODULE__} - " |> String.replace_leading("Elixir.", "")

	# TODO this is read at compile time
	@account_id System.fetch_env!("DNSIMPLE_ACCOUNT_ID")
	@server System.fetch_env!("DNSIMPLE_API")
	@base [
		base_url: @server <> "/#{@account_id}",
		auth: {:bearer, System.fetch_env!("DNSIMPLE_TOKEN")},
		retry: :never,
	]

	def get(%Req.Request{} = request) do
		case request(request) do
			{:ok, body} -> body
			{:error, :not_found} -> nil
		end
	end

	def request(%Req.Request{} = request) do
		Req.request(request, @base)
		# |> IO.inspect()
		|> print_rate_limit_info()
		|> case do
			{:ok, %Req.Response{status: 200, body: body}} -> {:ok, body}
			{:ok, %Req.Response{status: 201, body: body}} -> {:created, body}
			{:ok, %Req.Response{status: 202, body: body}} -> {:pending, body}
			{:ok, %Req.Response{status: 204}} -> :ok
			{:ok, %Req.Response{status: 400, body: body}} -> {:error, body}
			{:ok, %Req.Response{status: 401}} -> {:error, :unauthorized}
			{:ok, %Req.Response{status: 404}} -> {:error, :not_found}
			{:ok, %Req.Response{status: 429}} -> {:error, :rate_limited}
		end
	end

	# TODO create custom Req.Step
	defp print_rate_limit_info({:ok, %Req.Response{headers: headers}} = response) do
		headers = Enum.into(headers, %{})
		remaining = headers["x-ratelimit-remaining"]
		if remaining do
			limit = headers["x-ratelimit-limit"]
			reset = case headers["x-ratelimit-reset"] do
				nil -> nil
				value -> value |> String.to_integer() |> DateTime.from_unix!()
			end
			Logger.debug(@log <> "rate limit: #{remaining} remaining until #{reset} (#{limit}/h)")
		end

		response
	end
	defp print_rate_limit_info(response), do: response


end
