defmodule DeclarativeDNS.Providers.DigitalOcean.Zone do

	alias DeclarativeDNS.Data.Zone
	alias DeclarativeDNS.Providers.DigitalOcean.Client

	def list() do
		Req.new(url: "domains") |> Client.get()
	end

	def get(zone) when is_struct(zone, Zone) do
		Req.new(url: "domains/#{zone.name}") |> Client.get()
	end

	def records(zone) when is_struct(zone, Zone) do
		case Req.new(url: "domains/#{zone.name}/records") |> Client.request() do
			{:ok, %{"domain_records" => records}} -> {:ok, records}
			other -> other
		end
	end

	def apply(zone) when is_struct(zone, Zone) do
		Req.new(url: "domains/#{zone.name}")
		|> Client.request()
		|> case do
			{:ok, %{"domain" => %{"name" => name}}} -> {:ok, %{zone | name: name, fqdn: name}}
			{:error, :not_found} -> create(zone)
		end
	end

	defp create(zone) do
		[
			url: "domains",
			method: :post,
			json: %{
				"name" => zone.name,
			}
		]
		|> Req.new()
		|> Client.request()
		|> case do
			{:created, %{"domain" => %{"name" => name}}} ->  {:ok, %{zone | name: name, fqdn: name}}
			other -> other
		end
	end

	def delete(zone) when is_struct(zone, Zone) do
		Req.new(url: "domains/#{zone.name}", method: :delete)
		|> Client.request()
		|> case do
			:ok -> :ok
			{:error, :not_found} -> :ok
		end
	end

end
