defmodule DeclarativeDNS.Providers.DigitalOcean.Record do

	alias DeclarativeDNS.Data.Record
	alias DeclarativeDNS.Providers.DigitalOcean.Client

	def find_by_name_and_type(record) when is_struct(record, Record) do
		url = "domains/#{record.zone}/records?name=#{record.name}&type=#{record.type}"
		{:ok, %{"domain_records" => records}} =
			[url: url]
			|> Req.new()
			|> Client.request()

		records
	end

end
