defmodule DeclarativeDNS.Providers.DigitalOcean.Client do

	@base [
		base_url: "https://api.digitalocean.com/v2",
		auth: {:bearer, System.get_env("DO_TOKEN")},
	]

	def get(%Req.Request{} = request) do
		case request(request) do
			{:ok, body} -> body
			{:error, :not_found} -> nil
		end
	end

	def request(%Req.Request{} = request) do
		case Req.request(request, @base) do
			{:ok, %Req.Response{status: 200, body: body}} -> {:ok, body}
			{:ok, %Req.Response{status: 201, body: body}} -> {:created, body}
			{:ok, %Req.Response{status: 204}} -> :ok
			{:ok, %Req.Response{status: 401}} -> {:error, :unauthorized}
			{:ok, %Req.Response{status: 404}} -> {:error, :not_found}
			{:ok, %Req.Response{status: 429}} -> {:error, :rate_limited}
		end
	end

end
