defmodule DeclarativeDNS.Operator do
	use Bonny.Operator, default_watch_namespace: "default"

	step Bonny.Pluggable.Logger, level: :debug
	step DeclarativeDNS.Steps.DeletionPolicy
	step :delegate_to_controller
	step Bonny.Pluggable.ApplyStatus

	def crds() do
		[
			Bonny.API.CRD.new!(
				scope: :Namespaced,
				group: "declarative-dns.net",
				names: Bonny.API.CRD.kind_to_names("Contact"),
				versions: [DeclarativeDNS.API.V1Alpha1.Contact]
			),
			Bonny.API.CRD.new!(
				scope: :Namespaced,
				group: "declarative-dns.net",
				names: Bonny.API.CRD.kind_to_names("Record"),
				versions: [DeclarativeDNS.API.V1Alpha1.Record]
			),
			Bonny.API.CRD.new!(
				scope: :Namespaced,
				group: "declarative-dns.net",
				names: Bonny.API.CRD.kind_to_names("Zone"),
				versions: [DeclarativeDNS.API.V1Alpha1.Zone]
			),
		]
	end

	def controllers(_watch_namespace, _opts) do
		watch_namespace = System.fetch_env!("BONNY_NAMESPACE")

		[
			"Contact",
			"Record",
			"Zone",
		] |> Enum.map(fn kind ->
			%{
				query: K8s.Client.list("declarative-dns.net/v1alpha1", kind, namespace: watch_namespace),
				controller: DeclarativeDNS.Controller.Dispatcher,
			}
		end)
	end
end
