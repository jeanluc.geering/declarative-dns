defmodule DeclarativeDNS.Data.Domain do
	@keys [
		:contact_id,
		:name,
		auto_renew: true,
	]
	@enforce_keys [:name]
	@derive {Inspect, optional: [:contact_id, :auto_renew]}
	defstruct @keys

	@mapping (for value <- @keys, into: %{} do
		case value do
			{key, _} -> {Atom.to_string(key), key}
			key -> {Atom.to_string(key), key}
		end
	end)

	def new!(fields) when is_map(fields) do
		fields = fields
		|> Enum.map(fn
			{name, value} -> {Map.get(@mapping, name), value}
		end)
		|> Enum.filter(fn
			{nil, _} -> false
			_ -> true
		end)

		struct!(__MODULE__, fields)
	end
	def new!(fields), do: struct!(__MODULE__, fields)

end
