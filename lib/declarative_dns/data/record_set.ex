# aka resource record set
defmodule DeclarativeDNS.Data.RecordSet do
	@enforce_keys [:zone, :domain, :type]
	@derive {Inspect, optional: [:values, :raw]}
	defstruct [
		:zone,
		:domain,
		:type,
		values: [],
		ttl: 3600,
		raw: nil,
	]

	def new(fields), do: struct(__MODULE__, fields)
	def new!(fields), do: struct!(__MODULE__, fields)

end
