defmodule DeclarativeDNS.Data.Contact do
	@keys [
		:id,
		:first_name,
		:last_name,
		:organization_name,
		:job_title,
		:address1,
		:address2,
		:postal_code,
		:city,
		:state_province,
		:country,
		:email,
		:phone,
	]
	@enforce_keys [
		:first_name,
		:last_name,
		:address1,
		:postal_code,
		:city,
		:state_province,
		:country,
		:email,
		:phone,
	]
	# @derive {Inspect, optional: [:id, :fqdn]}
	defstruct @keys

	@mapping (for value <- @keys, into: %{} do
		case value do
			{key, _} -> {Atom.to_string(key), key}
			key -> {Atom.to_string(key), key}
		end
	end)

	def new!(fields) when is_map(fields) do
		fields = fields
		|> Enum.map(fn
			{name, value} -> {Map.get(@mapping, name), value}
		end)
		|> Enum.filter(fn
			{nil, _} -> false
			_ -> true
		end)

		struct!(__MODULE__, fields)
	end
	def new!(fields), do: struct!(__MODULE__, fields)

end
