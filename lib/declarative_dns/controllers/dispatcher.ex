defmodule DeclarativeDNS.Controller.Dispatcher do
	@moduledoc """
	DeclarativeDNS: Contact / Record / Zone controller.
	"""

	require Logger
	@log "#{__MODULE__} - " |> String.replace_leading("Elixir.", "")

	alias DeclarativeDNS.{Data, Provider}

	# TODO review: vs impl pluggable directly?
	use Bonny.ControllerV2

	# TODO review: here or in Operator
	step Bonny.Pluggable.SkipObservedGenerations
	step :handle_event

	@valid_actions [:add, :modify, :delete, :reconcile]

	@spec handle_event(Bonny.Axn.t(), Keyword.t()) :: Bonny.Axn.t()
	def handle_event(%Bonny.Axn{resource: resource, action: action} = axn, _opts) when action in @valid_actions do
		Logger.info(@log <> "#{action}: #{full_name(resource)}")

		%{"kind" => kind, "spec" => %{"provider" => provider_name} = spec} = resource

		module_name = kind_2_module(kind, provider_name)
		provider = provider_name |> Provider.lookup() |> Module.concat(module_name)
		data = Data |> Module.concat(module_name) |> apply(:new!, [spec])

		apply_or_delete = case action do
			:delete -> :delete
			_ -> :apply
		end

		# TODO error handling
		{:ok, _} = apply(provider, apply_or_delete, [data])

		# TODO review return value
		Bonny.Axn.success_event(axn)
	end

	defp full_name(%{"kind" => kind, "metadata" => %{"namespace" => ns, "name" => name}}) do
		"#{ns}/#{kind}/#{name}"
	end

	defp kind_2_module("Contact", _), do: Contact
	defp kind_2_module("Zone", _), do: Zone
	defp kind_2_module("Record", "dnsimple"), do: Record
	defp kind_2_module("Record", "ns1"), do: RecordSet

end
