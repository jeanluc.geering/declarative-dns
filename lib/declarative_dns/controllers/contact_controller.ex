# defmodule DeclarativeDNS.Controller.ContactController do
# 	require Logger
# 	@log "#{__MODULE__} - " |> String.replace_leading("Elixir.", "")

# 	require Bonny.API.CRD

# 	alias DeclarativeDNS.{Data, Provider}

# 	use Bonny.ControllerV2,
# 		for_resource:
# 			Bonny.API.CRD.build_for_controller!(
# 				names: Bonny.API.CRD.kind_to_names("Contact")
# 			),
# 		# check the controller guide for an explanation on skip_observed_generations.
# 		# If you enable skip_observed_generations, make sure to regenerate your manifest!
# 		skip_observed_generations: true

# 	@doc """
# 	Handles a `ADDED` event.
# 	"""
# 	@impl Bonny.ControllerV2
# 	@spec add(Bonny.Resource.t()) :: :ok | :error
# 	def add(%{} = resource) do
# 		Logger.info(@log <> "add: #{full_name(resource)}")
# 		apply(resource)
# 	end

# 	@doc """
# 	Handles a `MODIFIED` event
# 	"""
# 	@impl Bonny.ControllerV2
# 	@spec modify(Bonny.Resource.t()) :: :ok | :error
# 	def modify(%{} = resource) do
# 		Logger.info(@log <> "modify: #{full_name(resource)}")
# 		apply(resource)
# 	end

# 	@doc """
# 	Handles a `DELETED` event
# 	"""
# 	@impl Bonny.ControllerV2
# 	@spec delete(Bonny.Resource.t()) :: :ok | :error
# 	def delete(%{} = resource) do
# 		{provider, data} = get_spec(resource)
# 		case get_deletion_policy(resource) do
# 			"abandon" ->
# 				Logger.info(@log <> "skipping deletion of #{full_name(resource)}")
# 				:ok
# 			_ ->
# 				Logger.info(@log <> "delete: #{full_name(resource)}")
# 				apply(provider, :delete, [data])
# 		end
# 	end

# 	@doc """
# 	Called periodically for each existing CustomResource to allow for reconciliation.
# 	"""
# 	@impl Bonny.ControllerV2
# 	@spec reconcile(Bonny.Resource.t()) :: :ok | :error
# 	def reconcile(%{} = resource) do
# 		Logger.debug(@log <> "reconcile: #{full_name(resource)}")
# 		apply(resource)
# 	end

# 	defp apply(resource) do
# 		{provider, contact} = get_spec(resource)

# 		no_contact_id = is_nil(contact.id)

# 		contact = if no_contact_id do
# 			struct!(contact, id: get_in(resource, ["status", "contact", "id"]))
# 		else
# 			contact
# 		end

# 		case apply(provider, :apply, [contact]) do
# 			{:error, :not_found} ->
# 				{:error, "Contact[id:#{contact.id}] not found"}

# 			{:ok, contact} ->
# 				if no_contact_id do
# 					{:ok, put_in(resource, [Access.key("status", %{}), Access.key("contact", %{}), "id"], contact.id)}
# 					Bonny.Axn.update_status(axn, fn status ->
# 						put_in(status, [Access.key("contact", %{}), "id"], contact.id)
# 					end)

# 				else
# 					:ok
# 				end
# 		end
# 	end

# 	defp full_name(%{"metadata" => %{"namespace" => ns, "name" => name}}) do
# 		"Contact/#{ns}/#{name}"
# 	end

# 	defp get_spec(%{"spec" => %{"provider" => provider_name} = spec}) do
# 		provider = provider_name |> Provider.lookup() |> Module.concat(Contact)
# 		{provider, Data.Contact.new!(spec)}
# 	end

# 	defp get_deletion_policy(resource) do
# 		get_in(resource, ["metadata", "annotations", "declarative-dns.net/deletion-policy"])
# 	end

# end
