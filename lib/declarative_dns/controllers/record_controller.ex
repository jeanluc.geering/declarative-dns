# defmodule DeclarativeDNS.Controller.RecordController do
# 	require Logger
# 	@log "#{__MODULE__} - " |> String.replace_leading("Elixir.", "")

# 	require Bonny.API.CRD

# 	alias DeclarativeDNS.{Data, Provider}

# 	use Bonny.ControllerV2,
# 		for_resource:
# 			Bonny.API.CRD.build_for_controller!(
# 				names: Bonny.API.CRD.kind_to_names("Record")
# 			),
# 		# check the controller guide for an explanation on skip_observed_generations.
# 		# If you enable skip_observed_generations, make sure to regenerate your manifest!
# 		skip_observed_generations: true

# 	@doc """
# 	Handles a `ADDED` event.
# 	"""
# 	@impl Bonny.ControllerV2
# 	@spec add(Bonny.Resource.t()) :: :ok | :error
# 	def add(%{} = resource) do
# 		Logger.info(@log <> "add: #{full_name(resource)}")
# 		apply(resource)
# 	end

# 	@doc """
# 	Handles a `MODIFIED` event
# 	"""
# 	@impl Bonny.ControllerV2
# 	@spec modify(Bonny.Resource.t()) :: :ok | :error
# 	def modify(%{} = resource) do
# 		Logger.info(@log <> "modify: #{full_name(resource)}")
# 		apply(resource)
# 	end

# 	@doc """
# 	Handles a `DELETED` event
# 	"""
# 	@impl Bonny.ControllerV2
# 	@spec delete(Bonny.Resource.t()) :: :ok | :error
# 	def delete(%{} = resource) do
# 		{provider, data} = get_spec(resource)
# 		case get_deletion_policy(resource) do
# 			"abandon" ->
# 				Logger.info(@log <> "skipping deletion of #{full_name(resource)}")
# 				:ok
# 			_ ->
# 				Logger.info(@log <> "delete: #{full_name(resource)}")
# 				apply(provider, :delete, [data])
# 		end
# 	end

# 	@doc """
# 	Called periodically for each existing CustomResource to allow for reconciliation.
# 	"""
# 	@impl Bonny.ControllerV2
# 	@spec reconcile(Bonny.Resource.t()) :: :ok | :error
# 	def reconcile(%{} = resource) do
# 		Logger.debug(@log <> "reconcile: #{full_name(resource)}")
# 		apply(resource)
# 	end

# 	defp apply(resource) do
# 		{provider, record} = get_spec(resource)
# 		apply(provider, :apply, [record])
# 		:ok
# 	end

# 	defp full_name(%{"metadata" => %{"namespace" => ns, "name" => name}}) do
# 		"Record/#{ns}/#{name}"
# 	end

# 	defp get_spec(%{"spec" => %{"provider" => provider_name} = spec}) do
# 		provider = provider_name |> Provider.lookup() |> Module.concat(Record)

# 		# TODO move to DeclarativeDNS.Providers.MODULE.Record maybe
# 		record = case provider_name do
# 			"dnsimple" -> Data.Record.new!(spec)
# 			"ns1" -> Data.RecordSet.new!(spec)
# 		end

# 		{provider, record}
# 	end

# 	defp get_deletion_policy(resource) do
# 		get_in(resource, ["metadata", "annotations", "declarative-dns.net/deletion-policy"])
# 	end

# end
