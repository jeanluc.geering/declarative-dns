defmodule DeclarativeDNS.Controller.ZoneController do
	@moduledoc """
	DeclarativeDNS: ZoneController controller.

	## Controlled Resource

	You can pass the option `for_resource` to `use Bonny.ControllerV2`.
	If the option is ommitted, Bonny assumes the controler controls a
	custom resource tries to guess the custom resource's name
	from the controller's module name.
	If declared, `for_resource` can be:

	* a string representing the kind of a resource
	* a `%Bonny.API.CRD{}` struct
	* a `%Bonny.API.ResourceEndpoint` struct.

	### Examples

	```
	use Bonny.ControllerV2,
		for_resource:
			Bonny.API.CRD.build_for_controller!(
				names: Bonny.API.CRD.kind_to_names("CronTab"),
			)
	```

	```
	use Bonny.ControllerV2,
		for_resource:
			%Bonny.API.ResourceEndpoint{
				group: "apps",
				versions: "v1",
				resource_type: "deployments",
			}
	```

	## Declare RBAC permissions used by this module

	RBAC rules can be declared via the `rbac_rule/1` macro and generated using `mix bonny.manifest`.

	### Examples

	```
	# rbac_rule({apiGroup, resources_list, verbs_list})
	rbac_rule({"", ["pods", "secrets"], ["*"]})
	rbac_rule({"apiextensions.k8s.io", ["foo"], ["*"]})
	```
	"""
	require Logger
	@log "#{__MODULE__} - " |> String.replace_leading("Elixir.", "")

	alias DeclarativeDNS.{Data, Provider}

	use Bonny.ControllerV2

	step Bonny.Pluggable.SkipObservedGenerations
	step :handle_event

	@doc """
	Handles the action event
	"""
	@spec handle_event(Bonny.Axn.t(), Keyword.t()) :: Bonny.Axn.t()
	def handle_event(%Bonny.Axn{resource: resource, action: action} = axn, _opts) when action in [:add, :modify] do
		Logger.info(@log <> "#{action}: #{full_name(resource)}")
		apply(resource)
		Bonny.Axn.success_event(axn)
	end

	def handle_event(%Bonny.Axn{resource: resource, action: :delete} = axn, _opts) do
		{provider, data} = get_spec(resource)
		case get_deletion_policy(resource) do
			"abandon" ->
				Logger.info(@log <> "skipping deletion of #{full_name(resource)}")
				:ok
			_ ->
				Logger.info(@log <> "delete: #{full_name(resource)}")
				apply(provider, :delete, [data])
		end
		Bonny.Axn.success_event(axn)
	end

	def handle_event(%Bonny.Axn{resource: resource, action: :reconcile} = axn, _opts) do
		Logger.debug(@log <> "reconcile: #{full_name(resource)}")
		apply(resource)
		Bonny.Axn.success_event(axn)
	end

	defp apply(resource) do
		{provider, zone} = get_spec(resource)
		apply(provider, :apply, [zone])
		:ok
	end

	defp full_name(%{"metadata" => %{"namespace" => ns, "name" => name}}) do
		"Zone/#{ns}/#{name}"
	end

	defp get_spec(%{"spec" => %{"provider" => provider_name} = spec}) do
		provider = provider_name |> Provider.lookup() |> Module.concat(Zone)
		{provider, Data.Zone.new!(spec)}
	end

	defp get_deletion_policy(resource) do
		get_in(resource, ["metadata", "annotations", "declarative-dns.net/deletion-policy"])
	end

end
