import Config

config :bonny,
	# Function to call to get a K8s.Conn object.
	# The function should return a %K8s.Conn{} struct or a {:ok, %K8s.Conn{}} tuple
	get_conn: {DeclarativeDNS.K8sConn, :get, [config_env()]},

	# The namespace to watch for Namespaced CRDs.
	# Defaults to "default". `:all` for all namespaces
	# Also configurable via environment variable `BONNY_POD_NAMESPACE`
	# namespace: :all,
	# namespace: System.fetch_env!("BONNY_NAMESPACE"),

	# Set the Kubernetes API group for this operator.
	group: "declarative-dns.net",

	# Set the Kubernetes API versions for this operator.
	# This should be written in Elixir module form, e.g. YourOperator.API.V1 or YourOperator.API.V1Alpha1:
	versions: [DeclarativeDNS.API.V1Alpha1],

	# Name must only consist of only lowercase letters and hyphens.
	# Defaults to hyphenated mix app name
	operator_name: "declarative-dns",

	# Name must only consist of only lowercase letters and hyphens.
	# Defaults to hyphenated mix app name
	service_account_name: "declarative-dns",

	# Labels to apply to the operator's resources.
	labels: %{},

	# Operator deployment resources. These are the defaults.
	resources: %{limits: %{cpu: "200m", memory: "200Mi"}, requests: %{cpu: "200m", memory: "200Mi"}},

	manifest_override_callback: &Mix.Tasks.Bonny.Gen.Manifest.DeclarativeDNSCustomizer.override/1
