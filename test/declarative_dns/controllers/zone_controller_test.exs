defmodule DeclarativeDNS.Controller.ZoneControllerTest do
	@moduledoc false
	use ExUnit.Case, async: false
	alias DeclarativeDNS.Controller.ZoneController

	describe "add/1" do
		test "returns :ok" do
			event = %{}
			result = ZoneController.add(event)
			assert result == :ok
		end
	end

	describe "modify/1" do
		test "returns :ok" do
			event = %{}
			result = ZoneController.modify(event)
			assert result == :ok
		end
	end

	describe "reconcile/1" do
		test "returns :ok" do
			event = %{}
			result = ZoneController.reconcile(event)
			assert result == :ok
		end
	end

	describe "delete/1" do
		test "returns :ok" do
			event = %{}
			result = ZoneController.delete(event)
			assert result == :ok
		end
	end
end
